import {combineReducers} from "redux";
import { sessionReducer } from "./sessionreducer";

const appReducer = combineReducers({
    sessionReducer,
})

export default appReducer;