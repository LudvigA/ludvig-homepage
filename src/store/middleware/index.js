import { applyMiddleware } from "redux";
import { sessionmiddleware } from "./sessionmiddleware";

export default applyMiddleware(
    sessionmiddleware,
);