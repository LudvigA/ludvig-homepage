import{
    ACTION_SESSION_SET_LANGUAGE,
    ACTION_SESSION_ERROR,
    ACTION_SESSION_SUCCESS,
    sessionErrorAction,
    sessionSuccessAction,
} from "../actions/sessionActions"

export const sessionmiddleware =
  ({dispatch}) =>
  (next) =>
  async (action) => {
    next(action);

    if(action.type === ACTION_SESSION_SET_LANGUAGE){
        try {            
            localStorage.setItem("Language", JSON.stringify(""));
            dispatch(sessionSuccessAction())
        } catch{
            dispatch(sessionErrorAction(action.payload))
        }
    }
    
  };



