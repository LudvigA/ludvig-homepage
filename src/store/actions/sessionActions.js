export const ACTION_SESSION_SET_LANGUAGE = "[session] SET";
export const ACTION_SESSION_ERROR ="[session] ERROR";
export const ACTION_SESSION_SUCCESS="[session] SUCCESS";
export const ACTION_SESSION_UPDATE="[session] UPDATE"

export const sessionSetLanguageAction = (lang) => ({
    type: ACTION_SESSION_SET_LANGUAGE,
    payload: lang,
});

export const sessionSuccessAction = (success) => ({
    type: ACTION_SESSION_SUCCESS,
    payload: success,
});

export const sessionErrorAction = (error) => ({
    type: ACTION_SESSION_ERROR,
    payload: error
});

export const sessionUpdateAction = (update) => ({
    type: ACTION_SESSION_UPDATE,
    paylaod: update,
});