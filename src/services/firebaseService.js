const firebaseService = {

    postEmail: async (Email,Message,Name,Subject) => {
        const requestOptions = {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: Email,
            message: Message,
            name: Name,
            subject: Subject,
           
          })
        };
        const response = await fetch("https://ludvig-website-default-rtdb.firebaseio.com/message-list.json", requestOptions);
        return response.json();
      },
};
export default firebaseService;