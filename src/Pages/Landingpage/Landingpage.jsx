import styles from "./Landingpage.module.css";
import Header from "../../Components/Header";
import MyMain from "../../Components/MyMain";
import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { sessionSetLanguageAction } from "../../store/actions/sessionActions";

/**
 * Main page of the application containing all the content
 */
const Landingpage = () => {
  //State variable used to set what language should be displayed
  const [isnorwegian, SetIsNorwegian] = useState(true);
  const [active, setActive] = useState(true);
  const dispatch = useDispatch();

  //Onlick function that changes the state of isnorwegian based on its current state.
  const handleTranslateClick = () => {
    if (isnorwegian === true) {
      SetIsNorwegian(false);
      localStorage.setItem("Language", JSON.stringify("EN"));
    }
    if (isnorwegian === false) {
      SetIsNorwegian(true);
      localStorage.setItem("Language", JSON.stringify("NO"));
    }
  };
  useEffect(() => {
    const lang = JSON.parse(localStorage.getItem("Language"));
    if (!lang) {
      dispatch(sessionSetLanguageAction());
    }
    if (lang === "NO") {
      SetIsNorwegian(true);
    }
    if (lang === "EN") {
      SetIsNorwegian(false);
    }
  }, []);

  return (
    <div className={styles.container}>
      <Header
        active={active}
        setActive={setActive}
        isnorwegian={isnorwegian}
        handleTranslateClick={handleTranslateClick}
        className="d-flex flex-column"
      />
      <MyMain
        active={active}
        isnorwegian={isnorwegian}
        handleTranslateClick={handleTranslateClick}
        className="d-flex flex-column justify-content-center align-items-center leftmargin"
      />
    </div>
  );
};

export default Landingpage;
