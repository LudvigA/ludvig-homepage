import reactlogo from '../assets/img/react-logo.png'
import angularlogo from '../assets/img/angular-logo.png'
import azurelogo from '../assets/img/azure-logo.png'
import sqllogo from '../assets/img/sql-logo.png'
import vuelogo from '../assets/img/vue-logo.png'
import gitlogo from '../assets/img/git-logo-2color.png'
import csharplogo from '../assets/img/csharp-logo.png'
import pythonlogo from '../assets/img/python-logo.png'
import restapi from '../assets/img/rest-api.png'

/**
 * Component containing information about different technologies Ludvig has experience with.
 */
const Portfolio = (props) => {
    //State prop used to determine which language the content should be displayed in
    const isnorwegian = props.isnorwegian

    return(
        <section id="portfolio" className="portfolio section-bg">
      <div className="container">
        {isnorwegian ? (
        <div className="section-title">
          <h2>Kompetanse</h2>
          <p>En rekke av teknologiene Ludvig har erfaring med.</p>
        </div>
        ) : (
        <div className="section-title">
          <h2>Knowledge</h2>
          <p>A couple of the technologies Ludvig has experience with.</p>
        </div>
        )}
        <div className="row portfolio-container">
          <div className="col-lg-4 col-md-6 portfolio-item filter-app"data-aos="fade-right" data-aos-delay="150">
            <div className="portfolio-wrap">
              <img src={reactlogo} className="img-fluid" alt="" style={{width:"100px",height:"100px",}}/>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 portfolio-item filter-web"data-aos="fade-up" data-aos-delay="150">
            <div className="portfolio-wrap">
              <img src={angularlogo} className="img-fluid" alt="" style={{width:"100px",height:"100px",}}/>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 portfolio-item filter-app" data-aos="fade-left"data-aos-delay="150">
            <div className="portfolio-wrap">
              <img src={azurelogo} className="img-fluid" alt="" style={{width:"150px",height:"50px",}}/>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 portfolio-item filter-card" data-aos="fade-right" data-aos-delay="150">
            <div className="portfolio-wrap">
              <img src={sqllogo} className="img-fluid" alt="" style={{width:"100px",height:"100px",}}/>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 portfolio-item filter-web" data-aos="fade-up" data-aos-delay="150">
            <div className="portfolio-wrap">
              <img src={vuelogo} className="img-fluid" alt="" style={{width:"100px",height:"100px",}}/>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 portfolio-item filter-app" data-aos="fade-left" data-aos-delay="150">
            <div className="portfolio-wrap">
            <img src={gitlogo} className="img-fluid" alt="" style={{width:"200px",height:"100px",}}/>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 portfolio-item filter-card"data-aos="fade-right" data-aos-delay="150">
            <div className="portfolio-wrap">
              <img src={csharplogo} className="img-fluid" alt="" style={{width:"100px",height:"100px",}}/>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 portfolio-item filter-card" data-aos="fade-up" data-aos-delay="150">
            <div className="portfolio-wrap">
            <img src={pythonlogo} className="img-fluid" alt="" style={{width:"225px",height:"100px",}}/>
            </div>
          </div>
          
          <div className="col-lg-4 col-md-6 portfolio-item filter-web" data-aos="fade-left" data-aos-delay="150">
            <div className="portfolio-wrap">
            <img src={restapi} className="img-fluid" alt="" style={{width:"150px",height:"100px",}}/>
            </div>
          </div>
        </div>
      </div>
    </section>
    )
}
export default Portfolio;