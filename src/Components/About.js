import profilePicture from '../assets/img/Ludvig-profilbilde.jpg'
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
/**
 * Component displaying general information about Ludvig
 */
const About = (props) => {
  //State prop used to detect which language the content should be displayed in
  const isnorwegian = props.isnorwegian

    return(
        <section id="about" className="about">
      <div className="container">
      {isnorwegian ? (
        <div className="section-title">
          <h2>Om Ludvig</h2>
          <p>Ludvig har gjennomført Experis Academy Norge sitt program innen Fullstack .NET-utvikling, som gir omafattende backend- og frontend-kompetanse innen blant annet C#, SQL, API-Utvikling, Javascript og rammeverk som React, Angular og Vue. </p>
        </div>

      ):(
        <div className="section-title">
          <h2>About Ludvig</h2>
          <p>Ludvig has done a Fullstack .NET program at Experis Academy where he has gotten experience in both backend- and frontend-development. He been working with technologies such as C#, SQL, API-development, Javascript and frameworks such as React Angular and Vue</p>
        </div>
      )}
        
        <div className="row">
          <div className="col-lg-4" data-aos="fade-right">
            <img src={profilePicture} className="img-fluid" alt=""/>
          </div>
          <div className="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">
            <h3>Junior Fullstack .NET Developer</h3>
            <p className="fst-italic">
            </p>
            <div className="row">
              {isnorwegian ?(
                <div className="col-lg-6">
                <ul>
                  <li><KeyboardArrowRightIcon/><strong>Bursdag:</strong> <span>27 Juli 1995</span></li>
                  <li><KeyboardArrowRightIcon/> <strong>Mobil:</strong> <span>+47 9006 48 84</span></li>
                  <li><KeyboardArrowRightIcon/> <strong>By:</strong> <span>Bergen, Norge</span></li>
                </ul>
              </div>
              ):(
                <div className="col-lg-6">
                <ul>
                  <li><KeyboardArrowRightIcon/><strong>Birthday:</strong> <span>27 Juli 1995</span></li>
                  <li><KeyboardArrowRightIcon/> <strong>Phone:</strong> <span>+47 9006 48 84</span></li>
                  <li><KeyboardArrowRightIcon/> <strong>City:</strong> <span>Bergen, Norway</span></li>
                </ul>
              </div>
              )}
              {isnorwegian ?(
                <div className="col-lg-6">
                <ul>
                  <li><KeyboardArrowRightIcon/> <strong>Alder:</strong> <span>26</span></li>
                  <li><KeyboardArrowRightIcon/> <strong>Grader:</strong> <span>Bachelor: Informasjonsvitenskap<span><br/>Bachelor: Administrasjon og Organisasjonsvitenskap</span></span></li>
                  <li><KeyboardArrowRightIcon/> <strong>Email:</strong> <span>Justludi42@gmail.com</span></li>
                </ul>
              </div>
              ) :(
                <div className="col-lg-6">
                <ul>
                  <li><KeyboardArrowRightIcon/> <strong>Age:</strong> <span>26</span></li>
                  <li><KeyboardArrowRightIcon/> <strong>Degrees:</strong> <span>Bachelor: Informasjonsvitenskap<span><br/>Bachelor: Administrasjon og Organisasjonsvitenskap</span></span></li>
                  <li><KeyboardArrowRightIcon/> <strong>Email:</strong> <span>Justludi42@gmail.com</span></li>
                </ul>
              </div>
              )}
            </div>
            {isnorwegian?(
                <p>
                Ludvig har fullført en bachelorgrad i informasjonsvitenskap ved Universitetet i Bergen, i tillegg har han en bachelorgrad i administrasjon og organisasjonsvitenskap. Han tenker at han evt. kan kombinere disse to utdannelsene, og f.eks. jobbe med digitalisering av tjenester i offentlig sektor, men er fleksibel til andre bransjer. 
                </p>
            ):(
              <p>
                Ludvig has a bachelor in Information Science at The University of Bergen, in addition to that he has a bachelor degree in Administration and Organization Science. He thinks that he potentially could combine these degrees and for examble work on digitalizing services in the public sector, but are flexible to other buisnessses as well.
              </p>
            )}
          </div>
        </div>
      </div>
    </section>
    )
}
export default About