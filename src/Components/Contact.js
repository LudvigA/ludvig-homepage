import LocationOnIcon from '@mui/icons-material/LocationOn';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import styles from './Contact.module.css'
import firebaseService from '../services/firebaseService';
import { useState } from 'react';
/**
 * Component displaying Ludvigs Contact information
 * FIREBASE DOCUMENTATION: https://firebase.google.com/docs/reference/rest/database
 * FIREBASE URL for post and get : https://ludvig-website-default-rtdb.firebaseio.com/message-list.json
 * payload for firebase: POST
 * {
    "name": "john",
    "email": "john@john.com",
    "subject":"question",
    "message" :"i have a question"
}
 */
const Contact = (props) => {
    //State prop used to determine what language the content should be displayed in
    const isnorwegian = props.isnorwegian
    
    //State variables for Form
    const [name,SetName] = useState("")
    const [email,SetEmail] = useState("")
    const [subject, SetSubject] = useState("")
    const [message, SetMessage] = useState("")

    //onChange fucntions for form
    const onNameChange = (e) => {
      SetName(e.target.value)
    }
    const onEmailChange = (e) => {
      SetEmail(e.target.value)
    }
    const onSubjectChange = (e) => {
      SetSubject(e.target.value)
    }
    const onMessageChange = (e) => {
      SetMessage(e.target.value)
    }

    //Onclick function for form submit
    const onSubmitClick = async (e) => {
      e.preventDefault();
      if(window.confirm("Are you sure you want to send this message?")){
        await firebaseService.postEmail(email,message,name,subject);
      }
    }

    return(
      <div>
        <section id="contact" className={`contact`}>
          {isnorwegian ? (
            <div className="container">
            <div className="section-title">
              <h2>Kontakt</h2>
            </div>
            <div className="row">
              <div className="col-lg-5 d-flex align-items-stretch">
                <div className="info">
                  <div className="address">
                    <h4><LocationOnIcon className={styles.contactIcon}/>Adresse:</h4>
                    <p>Parkveien 34, 5006, BERGEN</p>
                  </div>
                  <div className="email">
                    <h4><MailOutlineIcon className={styles.contactIcon}/>Email:</h4>
                    <p>Justludi42@gmail.com</p>
                  </div>
                  <div className="phone">
                    <h4><PhoneAndroidIcon className={styles.contactIcon}/>Telefon:</h4>
                    <p>+47 9006 48 84</p>
                  </div>
                  <iframe title="location" style={{border: "0", width: "100%", height: "290px"}} allowFullScreen=""loading="lazy"src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJBZJUBqv-PEYRLHN6_XXvuyc&key=AIzaSyCaNQAGQ5bxx_hA6LXtRX8QUGKpPs9jsb0"></iframe>            
                  </div>
              </div>
            </div>
          </div>
          ) : (
          <div className="container">
          <div className="section-title">
            <h2>Contact</h2>
          </div>
          <div className="row">
            <div className="col-lg-5 d-flex align-items-stretch">
              <div className="info">
                <div className="address">
                  <h4><LocationOnIcon className={styles.contactIcon}/>Address:</h4>
                  <p>Parkveien 34, 5006, BERGEN</p>
                </div>
                <div className="email">
                  <h4><MailOutlineIcon className={styles.contactIcon}/>Email:</h4>
                  <p>Justludi42@gmail.com</p>
                </div>
                  <div className="phone">
                  <h4><PhoneAndroidIcon className={styles.contactIcon}/>Phone:</h4>
                  <p>+47 9006 48 84</p>
                </div>
                <iframe title="location" style={{border: "0", width: "100%", height: "290px"}} allowFullScreen=""loading="lazy"src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJBZJUBqv-PEYRLHN6_XXvuyc&key=AIzaSyCaNQAGQ5bxx_hA6LXtRX8QUGKpPs9jsb0"></iframe>            </div>
            </div>
            <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="name">Your Name</label>
                  <input onChange={onNameChange}type="text" name="name" class="form-control" id="name" required=""/>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Your Email</label>
                  <input onChange={onEmailChange} type="email" class="form-control" name="email" id="email" required=""/>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Subject</label>
                <input onChange={onSubjectChange} type="text" class="form-control" name="subject" id="subject" required=""/>
              </div>
              <div class="form-group">
                <label for="name">Message</label>
                <textarea onChange={onMessageChange} class="form-control" name="message" rows="10" required=""></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button onClick={onSubmitClick} type="submit">Send Message</button></div>
            </form>
          </div>
          </div>
        </div>
          )}
    </section>
    </div>
    )
}
export default Contact;