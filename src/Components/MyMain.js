import About from "./About"
import Contact from "./Contact"
import Facts from "./Facts"
import Portfolio from "./Portfolio"
import Resume from "./Resume"
import styles from "./Mymain.module.css"

/**
 * Parent Component
 */
const MyMain = (props) => {

    const active = props.active
    
    //State prop used to determine content language - passed down to child components
    const isnorwegian = props.isnorwegian
    return(
        <div className={active ? styles.MyMain : styles.MyMainNotActive}>
            <About isnorwegian={isnorwegian}/>
            <Facts isnorwegian={isnorwegian}/>
            <Resume isnorwegian={isnorwegian}/>
            <Portfolio isnorwegian={isnorwegian}/>
            <Contact isnorwegian={isnorwegian}/>
        </div>
    )
}

export default MyMain