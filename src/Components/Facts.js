import BrushIcon from '@mui/icons-material/Brush';
import MoodIcon from '@mui/icons-material/Mood';
import PermContactCalendarIcon from '@mui/icons-material/PermContactCalendar';
import PublishedWithChangesIcon from '@mui/icons-material/PublishedWithChanges';
import styles from './Facts.module.css'

/**
 * Component displaying key qualifications that Ludvig has
 */
const Facts = (props) => {
    //State prop used to determine which language the content should be displayed in.
    const isnorwegian = props.isnorwegian

    return(
      <section id="facts" className="facts">
        <div className="container">
          {isnorwegian ? (
          <div className="section-title">
            <h2>Nøkkelkvalifikasjoner</h2>
          </div>
          ) : (
          <div className="section-title">
            <h2>Key Qualifications</h2>
          </div>
         )}
        <div className="row no-gutters">
          <div className="col-lg-3 col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up">
            {isnorwegian ? (
              <div className="count-box">
                  <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" className="purecounter"></span>
                  <p><BrushIcon className={styles.factIcons}/><strong>Kreativ</strong></p>
                  <p>I gruppearbeid er Ludvig den kreative delen i teamet som kommer opp med mange gode idéer. Liker veldig godt idéfasen i prosjekter.</p>
              </div>
            ) : (
              <div className="count-box">
                <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" className="purecounter"></span>
                <p><BrushIcon className={styles.factIcons}/><strong>Creative</strong></p>
                <p>In group settings, Ludvig is creative part of the team that comes up with many good ideas. He really likes the ideation pahse of projects.</p>
            </div>
            )}
          </div>
          <div className="col-lg-3 col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up" data-aos-delay="100">
            {isnorwegian ? (
                <div className="count-box">
                <p><PermContactCalendarIcon className={styles.factIcons}/><strong>Pålitelig</strong></p>
                <p>Holder avtaler.</p>
              </div>
            ) : (
              <div className="count-box">
              <p><PermContactCalendarIcon className={styles.factIcons}/><strong>Reliable</strong></p>
              <p>Holds his word</p>
            </div>
            )}
          </div>
          <div className="col-lg-3 col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up" data-aos-delay="200">
            {isnorwegian ? (
                <div className="count-box">
                  <p><MoodIcon className={styles.factIcons}/><strong>Omgjengelig</strong></p>
                  <p>Liker å bruke humor og skape god stemning i gruppen.</p>
              </div>
            ) : (
            <div className="count-box">
              <p><MoodIcon className={styles.factIcons}/><strong>Sociable</strong></p>
              <p>Likes to use humor to create enjoyable working enviroment</p>
            </div>
            )}
          </div>
          <div className="col-lg-3 col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up" data-aos-delay="300">
            {isnorwegian ? (
            <div className="count-box">
              <p><PublishedWithChangesIcon className={styles.factIcons}/><strong>Tilpasningsdyktig</strong></p>
              <p>Jobber godt selvstendig og trives også godt i teamarbeid. Nysjerrig på å tilegne seg ny kunnskap og utfordre seg selv</p>
            </div>
            ) : (
            <div className="count-box">
              <p><PublishedWithChangesIcon className={styles.factIcons}/><strong>Adaptable</strong></p>
              <p>Works well on his own, but also enjoys working in teams. Curious about acquiring new knowledge and challenge himself</p>
            </div>
            )}
          </div>
        </div>
      </div>
    </section>
    )
}
export default Facts