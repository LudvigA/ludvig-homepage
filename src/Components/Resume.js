/**
 * Resume component contaning Education and work experience
 */
const Resume = (props) => {
    //State prop used to determine what language the content should be displayed in.
    const isnorwegian = props.isnorwegian

    return(
        <section id="resume" className="resume">
      <div className="container">
        {isnorwegian ? (
        <div className="section-title">
          <h2>Utdanning og Jobberfaring</h2>
        </div>
        ) : (
        <div className="section-title">
          <h2>Education and Work Experience</h2>
        </div>
        )}
          {isnorwegian ? (
          <div className="row">
            <div className="col-lg-6" data-aos="fade-up">
              <h3 className="resume-title">Utdanning</h3>
            
            <div className="resume-item">
              <h4>Bachelor: Informasjonsvitenskap</h4>
              <h5>2019 - 2021</h5>
              <p><em>Universitetet i Bergen</em></p>
              <p>Studiet ruster en til å forstå hvordan en kan nytte informasjons- og kommunikasjonsteknologi på en god måte. En får en særlig fordypning i informasjonssystem, menneske-maskin interaksjon og kunstig intelligens. En får et teoretisk og praktisk grunnlag for å analysere behov og utvikle gode dataløsninger for et bredt spekter av virksomheter. Bachelorprogrammet i informasjonsvitenskap gir en muligheten til å prøve seg som utvikler, designer eller prosjektleder.</p>
            </div>
            <div className="resume-item">
              <h4>Bachelor: Administrasjon og Organisasjonsvitenskap</h4>
              <h5>2014 - 2017</h5>
              <p><em>Universitetet i Bergen</em></p>
              <p>I Administrasjon og Organisasjonsvitenskap opparbeider en seg evne til å forstå prosesser og samhandlinger internt i organisasjoner og mellom organisasjoner. I tillegg får en kunnskap om politikk og forvaltningssystem.</p>
            </div>
          </div>
          <div className="col-lg-6" data-aos="fade-up" data-aos-delay="100">
            <h3 className="resume-title">Jobberfaring</h3>
            <div className="resume-item">
              <h4>Experis Academy</h4>
              <h5>2021 - NÅ</h5>
              <p><em>Junior Fullstack .NET Utvikler </em></p>
              <ul>
                <li>Vært gjennom et intensivt 3-måneders opplæringsprogram hvor han har lært en rekke teknologier i Backend og Frontend. Er nå åpen for oppdrag ute hos kunder.</li>
              </ul>
            </div>
            <div className="resume-item">
              <h4>Pedagogisk Vikarsentral</h4>
              <h5>2017 - 2019</h5>
              <p><em>Assistent</em></p>
              <ul>
                <li>Har jobbet som assistent i barnhage, barneskole og ungdomsskole.</li>
              </ul>
            </div>
            <div className="resume-item">
              <h4>Aftenskolen Rogaland</h4>
              <h5>2012 - 2012</h5>
              <p><em>Kontormedarbeider - Vikariat</em></p>
              <ul>
                <li>Forefallende kontorarbeid som gikk ut på sortering av dokumenter og klargjøring av kursplaner.</li>
              </ul>
            </div>
          </div>
          </div>
          ) : (
            <div className="row">
            <div className="col-lg-6" data-aos="fade-up">
            <h3 className="resume-title">Eductaion</h3>
            <div className="resume-item">
              <h4>Bachelor: Information Science</h4>
              <h5>2019 - 2021</h5>
              <p><em>University of Bergen</em></p>
              <p>This in this course you learn how to use informtaion- and communication technologies in a good way. You especially get a deeper knowledge about informationsystems, human-computer interaction and artificial intelligence. You get a theoretical and practical foundation to analyse needs and develop good soulutions for a wide range of buisnesses. The bachelor program in information science gives the candidate an opportunity to work as a developer, designer or projectleader.</p>
            </div>
            <div className="resume-item">
              <h4>Bachelor: Administration and Organization Science</h4>
              <h5>2014 - 2017</h5>
              <p><em>University of Bergen</em></p>
              <p>I Administration and Organization Science the candidate learns the ability to understand processes and interactions in organizations and between them. In addition to the you gain knowledge about politics and management systems</p>
            </div>
          </div>
          <div className="col-lg-6" data-aos="fade-up" data-aos-delay="100">
            <h3 className="resume-title">Working Experience</h3>
            <div className="resume-item">
              <h4>Experis Academy</h4>
              <h5>2021 - NOW</h5>
              <p><em>Junior Fullstack .NET Developer </em></p>
              <ul>
                <li>Been through an intensive 3-month learning program, where he has learned a wide range of technologies in backend and frontend. Is now open for job assignments</li>
              </ul>
            </div>
            <div className="resume-item">
              <h4>Pedagogisk Vikarsentral</h4>
              <h5>2017 - 2019</h5>
              <p><em>Assistant</em></p>
              <ul>
                <li>Har jobbet som assistent i barnhage, barneskole og ungdomsskole.</li>
                <li>Has worked as an assisent i kindergarten, primary school and junior high school</li>
              </ul>
            </div>
            <div className="resume-item">
              <h4>Aftenskolen Rogaland</h4>
              <h5>2012 - 2012</h5>
              <p><em>Office worker - Substitute</em></p>
              <ul>
                <li>Done different tasks in the office, such as sotrting documents and getting course plans ready for distribution</li>
              </ul>
            </div>
          </div>
          </div>
          )}
      </div>
    </section>
    )
}
export default Resume;