import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import Typed from 'react-typed'
import profilePicture from '../assets/img/Ludvig-profilbilde2.jpg'
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import GitHubIcon from '@mui/icons-material/GitHub';
import HomeIcon from '@mui/icons-material/Home';
import MailIcon from '@mui/icons-material/Mail';
import PersonIcon from '@mui/icons-material/Person';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import SchoolIcon from '@mui/icons-material/School';
import styles from "./Header.module.css"
import AOS from 'aos';
import ReactTooltip from "react-tooltip";
import { useEffect } from "react";
import MenuIcon from '@mui/icons-material/Menu';
import CloseIcon from '@mui/icons-material/Close';
import "aos/dist/aos.css"
/**
 * Header component containing a Navbar and Hero-Section
 */
const Header = (props) => {
  //State prop used to determine which language the content should be displayed in
  const isnorwegian = props.isnorwegian
  const active = props.active
  const setActive= props.setActive
  //State used to show hide navbar
  

  //UseEffect that determine the duration of the AOS animations
  useEffect(() => {
    AOS.init({
      duration : 1000
    });
  }, []);

    const handleActive = () => {
      if (active) {
        setActive(false)
      } else {
        setActive(true)
      }
    };
  
    return (
<>

    <header id="header" className={active ? `${styles.header} ${styles.open}`: styles.header}>
    <div className="d-flex flex-column">
      <div className="profile">
        <img src={profilePicture} alt="" className="img-fluid rounded-circle" style={{width:"100px",height:"100px",}}/>
        <h1 className="text-light"><a href="#hero">Ludvig Ånestad</a></h1>
        <div onClick={handleActive} className={styles.icon}> 
        <CloseIcon className={styles.innericon}/>
        </div>
        {isnorwegian ? (
        <div className="social-links mt-3 text-center">
          <a href="https://gitlab.com/LudvigA" className="google-plus"><GitHubIcon/></a>
          <a href="https://www.linkedin.com/in/ludvig-%C3%A5nestad-ab24b5207/" className="linkedin"><LinkedInIcon/></a>
          <button data-tip data-for="TranslateENTip" className={styles.translateBtn} href="#" onClick={props.handleTranslateClick}>NO/EN</button>
          <ReactTooltip id="TranslateENTip" place="bottom" effect="solid" >
            Select Language
          </ReactTooltip>
        </div>
          ) : (
          <div className="social-links mt-3 text-center">
            <a href="https://gitlab.com/LudvigA" className="google-plus"><GitHubIcon/></a>
            <a href="https://www.linkedin.com/in/ludvig-%C3%A5nestad-ab24b5207/" className="linkedin"><LinkedInIcon/></a>
            <button data-tip data-for="TranslateNOTip" className={styles.translateBtn} href="#" onClick={props.handleTranslateClick}>NO/EN</button>
            <ReactTooltip id="TranslateNOTip" place="bottom" effect="solid" >
              Select Language
            </ReactTooltip>
          </div>
          )}
      </div>
      {isnorwegian ? (
      <nav id="navbar" className="nav-menu navbar">
        <ul>
          <li><a href="#hero" className="nav-link scrollto active"><HomeIcon className={styles.headerIcons}/> <span>Hjem</span></a></li>
          <li><a href="#about" className="nav-link scrollto"><PersonIcon className={styles.headerIcons}/> <span>Om Ludvig</span></a></li>
          <li><a href="#resume" className="nav-link scrollto"><SchoolIcon className={styles.headerIcons}/> <span>Utdanning og Jobberfaring</span></a></li>
          <li><a href="#portfolio" className="nav-link scrollto"><MenuBookIcon className={styles.headerIcons}/> <span>Kompetanse</span></a></li>
          <li><a href="#contact" className="nav-link scrollto"><MailIcon className={styles.headerIcons}/> <span>Kontakt</span></a></li>
       </ul>
      </nav>
      ):(
        <nav id="navbar" className="nav-menu navbar">
        <ul>
          <li><a href="#hero" className="nav-link scrollto active"><HomeIcon className={styles.headerIcons}/> <span>Home</span></a></li>
          <li><a href="#about" className="nav-link scrollto"><PersonIcon className={styles.headerIcons}/> <span>About Ludvig</span></a></li>
          <li><a href="#resume" className="nav-link scrollto"><SchoolIcon className={styles.headerIcons}/> <span>Education og Work Experience</span></a></li>
          <li><a href="#portfolio" className="nav-link scrollto"><MenuBookIcon className={styles.headerIcons}/> <span>Knowledge</span></a></li>
          <li><a href="#contact" className="nav-link scrollto"><MailIcon className={styles.headerIcons}/> <span>Contact</span></a></li>
        </ul>
      </nav>
      )}
    </div>
  </header>
  <div className={styles.parallax}>
    <section id="hero" className="d-flex flex-column justify-content-center align-items-center" >
    <div className='hero' data-aos="fade-in">
      <h1 className={styles.heroTitle}>Ludvig Ånestad</h1>
      {isnorwegian ? (
          <p>Jeg er en <span className="typed">
          <Typed
              strings={['Designer','Backend Utvikler','Frontend Utvikler',]}
              backSpeed={50}
              typeSpeed={40}
              loop/>
              </span></p>
    ):(
      <p>I'm a <span className="typed">
      <Typed
          strings={['Designer','Backend Developer','Frontend Developer',]}
          backSpeed={50}
          typeSpeed={40}
          loop/>
          </span></p>
    )}
  </div>
</section>
</div>
{!active && (
        <div onClick={handleActive} className={styles.icon}>
          <MenuIcon className={styles.innericon}/>
        </div>
      )}
</>
    )
} 
export default Header