import "./App.css";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Landingpage from "./Pages/Landingpage/Landingpage";

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <Redirect to="/landingpage" />
        </Route>
        <Route path="/landingpage" component={Landingpage} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
